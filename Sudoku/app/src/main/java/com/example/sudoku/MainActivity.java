package com.example.sudoku;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements
        View.OnClickListener {
    public static final String TAG = "Sudoku";
    private int game_continue = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button continueButton = (Button) findViewById(R.id.continue_button);
        continueButton.setOnClickListener(this);
        Button newButton = (Button) findViewById(R.id.new_button);
        newButton.setOnClickListener(this);
        Button aboutButton = (Button) findViewById(R.id.about_button);
        aboutButton.setOnClickListener(this);
        Button exitButton = (Button) findViewById(R.id.exit_button);
        exitButton.setOnClickListener(this);
    }
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.continue_button:
                startGame(game_continue);
                break;
            case R.id.about_button:
                Intent i = new Intent(this, About.class);
                startActivity(i);
                break;
            case R.id.new_button:
                openNewGameDialog();
                break;
            case R.id.exit_button:
                finish();
                break;
        }
    }
    class DialogOnClickListener implements DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialoginterface,int i) {
            game_continue = i;
        }
    }
    /*Ask the user what difficulty level they want*/
    private void openNewGameDialog() {
        DialogOnClickListener listener = new DialogOnClickListener();
        AlertDialog.Builder alertDiag = new AlertDialog.Builder(this);
        alertDiag.setTitle(R.string.new_game_title);
        alertDiag.setItems(R.array.difficulty, listener);
        alertDiag.show();
    }
    /*Start a new game with the given difficulty level*/
    private void startGame(int i) {
        Log.i(TAG, "clicked on " + i);
    }
}